# aolko's userscripts

This is a repository for my userscripts.

Please be kind and read the [wiki](https://codeberg.org/aolko/userscripts/wiki/) for more information.

## News/announcements
**13.02.2024**
Will start working on 1.x branch of PH Toolbox soon. [Here are the tasks](https://codeberg.org/aolko/userscripts/issues/7).

**18.10.2023**
Another nav redesign by PH, so far so good

**20.07.2023**
PH Toolbox 0.6.5+ is compatible with partial PH redesign.

**08.07.2023**
Warning: PH Toolbox is **not** compatible with partial PH redesign yet.

**21.12.2022**
Be sure to leave your feedback on PH toolbox via [issues](https://codeberg.org/aolko/userscripts/issues).